.. Parallel Bilby documentation master file, created by
   sphinx-quickstart on Tue Feb 11 11:49:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

|Pypi| |Conda|  |Gitlab|

==========================================
Parallel Bilby
==========================================
Solving your inference needs at flaming new speeds

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   at_a_glance
   installation
   data_generation
   data_analysis
   examples
   citing_parallel_bilby


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |PyPI| image:: https://img.shields.io/pypi/v/parallel_bilby?label=Parallel%20Bilby&logo=Parallel%20Bilby
   :alt: PyPI
   :target: https://pypi.org/project/parallel_bilby/

.. |Conda| image:: https://img.shields.io/conda/vn/conda-forge/parallel-bilby?color=green
   :alt: Conda
   :target: https://anaconda.org/conda-forge/parallel-bilby

.. |Gitlab| image:: https://img.shields.io/badge/git.ligo-master-orange?style=flat&logo=gitlab
   :alt: Gitlab
   :target: https://git.ligo.org/lscsoft/parallel_bilby

